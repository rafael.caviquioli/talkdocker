FROM node:9.11.2 as node_builder
RUN mkdir /app
WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
RUN npm run build

# -----------------------------------------------------------------

FROM nginx:1.13.9-alpine
COPY --from=node_builder /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]