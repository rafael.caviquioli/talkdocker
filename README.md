#### Build production image

```
docker build -t docker-talk . -f Prod.Dockerfile
```

#### Build development image

```
docker run -v /home/rafael/Downloads/docker-talk:/home/node \
    -w /home/node \
    -p 80:3000 \
    node:9.11.2 npm start
```





#### Backend challenge

Image to build: mcr.microsoft.com/dotnet/core/sdk:2.2
Image to run: mcr.microsoft.com/dotnet/core/aspnet:2.2

```
dotnet publish docker-talk-backend.csproj -c Release -o ./publish
```